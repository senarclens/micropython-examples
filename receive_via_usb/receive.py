#!/usr/bin/env python3
"""
Receive data from a Raspberry Pi Pico on a Linux host.

The data on the pico can simply be sent via `print` to
the default output.

Note that nothing is received when the Pico logs to a
repl console. Run the program "headless" (ie without
Thonny et al. to write to the host's serial console.
"""

import os
import serial
import sys
import time

PICO_SERIAL = '/dev/ttyACM0'

if os.path.exists(PICO_SERIAL) == True:
    ser = serial.Serial(PICO_SERIAL, 115200)
else:
    print(f'cannot connect to {PICO_SERIAL}, exiting...')
    sys.exit(1)

while True:
    time.sleep(0.1)  # same as the sending interval on pico
    if ser.inWaiting() > 0:
        pico_data = ser.readline().decode("utf-8")
        print(pico_data.strip())
