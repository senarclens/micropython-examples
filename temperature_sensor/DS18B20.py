import machine, onewire, ds18x20
from time import sleep
from machine import Pin

ds_sensor = ds18x20.DS18X20(onewire.OneWire(Pin(26)))

roms = ds_sensor.scan()
print('Found DS devices: ', roms)

while True:
  ds_sensor.convert_temp()
  sleep(0.75)
  for rom in roms:
    print(ds_sensor.read_temp(rom))
  sleep(1)
