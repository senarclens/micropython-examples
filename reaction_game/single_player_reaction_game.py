"""
A single player reaction game.

To start the game, press the `start` button.
When started, an LED will light for a few seconds. When the LED is switched
off, the player must press a button as soon as possible. The game will print
the player's reaction time.

To play again, the player may use the start button again.
"""

import machine
import utime
import urandom

intled = machine.Pin(15,machine.Pin.OUT)
button = machine.Pin(14,machine.Pin.IN, machine.Pin.PULL_DOWN)
button_start = machine.Pin(16,machine.Pin.IN, machine.Pin.PULL_DOWN)


def button_pressed(pin):
    global was_pressed
    if not was_pressed:
        was_pressed = True
        timer_end=utime.ticks_ms()
        duration= utime.ticks_diff(timer_end, timer_start)
        print("Your reactiontime was:", duration, "ms")


def button_reset(pin):
    global was_pressed
    global timer_start
    was_pressed = False
    intled.high()
    utime.sleep(urandom.uniform(3,6))
    intled.off()
    timer_start=utime.ticks_ms()

button_start.irq(trigger=machine.Pin.IRQ_RISING,
              handler=button_reset)
button.irq(trigger=machine.Pin.IRQ_RISING,
           handler=button_pressed)
