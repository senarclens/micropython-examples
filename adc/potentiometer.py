"""
Read values from a 10 kOhm potentiometer.

Wire one leg agains 3V3 and the other agains one of the Pico's
ADC Pins.
The lower the resistance, the higher the voltage on the ADC Pin.
Low resistances result in a max. value of 65535 (~3.287 V).
High resistances still have a considerable voltage left at the ADC (~3.284)
and result in values just below 65000.

When wiring the third leg of the potentiometer against ground, the
potentiometer acts as voltage divider. Now, in case of higher resistances,
there is almost zero resistance between the output and ground and hence
the ADC reads values of only a few hundret (close to 0 volts).
"""


from machine import ADC
import utime

potentiometer = ADC(26)
VOLT_PER_TICK = 3.3 / 65535

while True:
    value = potentiometer.read_u16()
    print(value, value * VOLT_PER_TICK, 'V')
    utime.sleep(2)
