"""
Read the temperature from the RP2040's internal temperature.

The formula to calculate the temperature from the voltage on
ADC 4 is:
T = 27 - (ADC_voltage - 0.706) / 0.001721
This formula is specific to the RP2040's temperature sensor.
"""


from machine import ADC
import utime

temp_sensor = ADC(4)
VOLT_PER_TICK = 3.3 / 65535

def convert_to_celsius(voltage):
    """Convert RP2040 temperature sensor voltage into degrees celsius."""
    return 27 - (voltage - 0.706) / 0.001721

while True:
    reading = temp_sensor.read_u16()
    voltage = reading * VOLT_PER_TICK
    print(reading, voltage, 'V', convert_to_celsius(voltage), 'C')
    utime.sleep(2)
