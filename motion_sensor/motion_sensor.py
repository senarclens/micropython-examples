"""
Read motion detection data from an HC-SR501.

The HC-SR501 is a passive infrared (PIR) sensor.

The alarm will go off when any motion is detected and blink an LED
for `ALARM_DURATION_S` seconds. If another alarm will be triggered
during the first alarm, the LED blinking duration will be reset to
start from the beginning.
I.e. it is possible to keep the LED alarm on constantly, by triggering
multiple alarms. However, a new alarm can only be triggered after
about three seconds due to the HC-SR501 having a short timeout
between alarms.
"""

import machine
import utime

SENSOR_GPIO_PIN = 0
LED_GPIO_PIN = 25
DEBOUNCING_DELAY_MS = 100
ALARM_DURATION_S = 15

sensor = machine.Pin(SENSOR_GPIO_PIN, machine.Pin.IN, machine.Pin.PULL_DOWN)
led = machine.Pin(LED_GPIO_PIN, machine.Pin.OUT)

remaining_alarm_time = 0

def motion_handler(pin):
    """
    React if a motion was detected.
    
    There needs to be constant movement for a little while, otherwise the
    triggering signal will be discarded. To do so, the sensor is read
    after waiting a very little delay - a process called "debouncing".
    """
    utime.sleep_ms(DEBOUNCING_DELAY_MS)
    if pin.value():
        global remaining_alarm_time
        print("Motion detected. Triggering alarm and further actions.")
        remaining_alarm_time = ALARM_DURATION_S


sensor.irq(trigger=machine.Pin.IRQ_RISING, handler=motion_handler)
print("IRQ handler is waiting for motion.")
while True:
    while remaining_alarm_time > 0:
        led.on()
        utime.sleep(0.5)
        led.off()
        utime.sleep(0.5)
        remaining_alarm_time -= 1
        print("remaining_alarm_time:", remaining_alarm_time)
    utime.sleep(1)