"""
Write text to a connected 16x2 LCD.

Connect the display to the pins defined below or adjust the script.
"""
SDA_PIN = 0
SCL_PIN = 1
FREQUENCY = 400000
LINES = 2
COLUMNS = 16

from machine import Pin, I2C
from machine_i2c_lcd import I2cLcd

i2c = I2C(0, sda=Pin(SDA_PIN), scl=Pin(SCL_PIN), freq=FREQUENCY)
# Read the id of an LCD on the I2C (inter integrated circuit) bus.
print('Connected devices:', i2c.scan())

display_address = i2c.scan()[0]  # if only one item is connected

lcd = I2cLcd(i2c, display_address, LINES, COLUMNS)
# lcd.show_cursor()
# lcd.blink_cursor_on()
lcd.putstr('Hello, World!\nLCD works :)')
