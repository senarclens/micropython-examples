# Use a Cheap 16x2 LCD display

## Installation
The provided driver files `i2c_lcd.py` and `machine_i2c_lcd.py` are copies
of https://github.com/dhylands/python_lcd/tree/master/lcd.
Both are using the MIT license.

## Programs
The most simple program is `hello_display.py` which just displays a static
message on the LCD.
