"""
Write text to a connected 16x2 LCD.

Connect the display to the pins defined below or adjust the values.

DHT22
 __
/  \
####   1: 3.3 V
####   2: data (has to be pulled up)
####   3: -
####   4: GND
||||
1234

MH-Z19C uses 5 volt input. Connect sensor's Rx to Pico's Tx and vice versa.

The data is also printed as this allows it to be read from a host PC
via the serial interface /dev/ttyACM0.
Note that reading from /dev/ttyACM0 only works if this program runs
"headless", i.e. as `main.py` without a running Thonny et al. editor instance.
"""
SDA_PIN = 0
SCL_PIN = 1
FREQUENCY = 400000
LINES = 2
COLUMNS = 16
DHT22_PIN = 16
UART_TX_PIN = 12
UART_RX_PIN = 13

import utime
from machine import Pin, I2C

from machine_i2c_lcd import I2cLcd
from DHT22 import DHT22
from mh_z19 import MH_Z19

i2c = I2C(0, sda=Pin(SDA_PIN), scl=Pin(SCL_PIN), freq=FREQUENCY)
# Read the id of an LCD on the I2C (inter integrated circuit) bus.
display_address = i2c.scan()[0]  # if only one item is connected

lcd = I2cLcd(i2c, display_address, LINES, COLUMNS)
dht_sensor=DHT22(Pin(DHT22_PIN, Pin.IN, Pin.PULL_UP))
co2_sensor = MH_Z19(Pin(UART_TX_PIN), Pin(UART_RX_PIN))

print("#Temperature in C, relative humidity in %, CO2 in PPM")
while True:
    T,H = dht_sensor.read()
    lcd.clear()
    try:
        co2 = co2_sensor.read_co2()
    except:
        co2 = None
    if T is None or co2 is None:
        lcd.putstr('sensor error')
    else:
        lcd.putstr("{:4.1f}C    {:3.1f}%\n    {:4d}PPM".format(T,H,co2))
        print("{:.1f},{:.1f},{}".format(T,H,co2))
    #DHT22 not responsive if delay too short
    utime.sleep_ms(1000)
